package com.example.ecomerce.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "prixAchat" })
@Entity
@Table(name = "Product")
public class Product {
	@Id
	@GeneratedValue
	private int id;
	@Length(min = 3, max = 20, message = "ici")
	private String nom;
	private int prix;
	@JsonIgnore
	private int prixAchat;

	@ManyToOne
	@JoinColumn
	private Categorie Categorie;

	public Product() {
	}

	public Product(int id, @Length(min = 3, max = 20, message = "ici") String nom, int prix, int prixAchat,
			com.example.ecomerce.model.Categorie categorie) {
		super();
		this.id = id;
		this.nom = nom;
		this.prix = prix;
		this.prixAchat = prixAchat;
		Categorie = categorie;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public int getPrixAchat() {
		return prixAchat;
	}

	public void setPrixAchat(int prixAchat) {
		this.prixAchat = prixAchat;
	}

	public Categorie getCategorie() {
		return Categorie;
	}

	public void setCategorie(Categorie categorie) {
		Categorie = categorie;
	}

}