package com.example.ecomerce.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.ecomerce.model.Categorie;

@Repository
public interface CategorieRepository  extends JpaRepository<Categorie, Long>{
	public Categorie findById (int id);
}
