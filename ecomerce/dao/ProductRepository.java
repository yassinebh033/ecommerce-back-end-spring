package com.example.ecomerce.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.ecomerce.model.Product;


@Repository
public interface ProductRepository extends JpaRepository<Product,Integer> {
	Product findById(int id);
	List<Product> findByPrixGreaterThan(int prixLimit);
	@Query("SELECT p FROM Product p WHERE p.prix > :prixLimit")
	List<Product> chercherbyPrix(@Param("prixLimit")int prix);
	@Query("select p.nom from Product p inner join p.Categorie ca where ca.id = p.id AND ca.nom=:name ")
	List<String> chercherByName(@Param("name")String name);
	  @Transactional
	    @Modifying
	    @Query("UPDATE Product p SET p.prix =:prix WHERE p.id=:id")
	    public void updatePrix(@Param("id") Integer id,@Param("prix") Double prix);
}
