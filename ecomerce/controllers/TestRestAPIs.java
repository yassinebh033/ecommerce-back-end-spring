package com.example.ecomerce.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
 
@RestController
public class TestRestAPIs {
  
  @GetMapping("/api/test/user")
  @PreAuthorize("hasRole('user') or hasRole('admin')")
  public String userAccess() {
    return ">>> User Contents!";
  }
  
  @GetMapping("/api/test/pm")
  @PreAuthorize("hasRole('pm') or hasRole('admin')")
  public String projectManagementAccess() {
    return ">>> Board Management Project";
  }
  
  @GetMapping("/api/test/admin")
  @PreAuthorize("hasRole('admin')")
  public String adminAccess() {
    return ">>> Admin Contents";
  }
}