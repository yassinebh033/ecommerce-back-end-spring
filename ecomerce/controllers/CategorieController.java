package com.example.ecomerce.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.example.ecomerce.dao.CategorieRepository;
import com.example.ecomerce.exception.CategorieIntrouvableException;
import com.example.ecomerce.model.Categorie;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value="/rest/api")
@CrossOrigin(origins = "http://localhost:4200")
public class CategorieController {
    @Autowired
    CategorieRepository categorieRepository;

    @GetMapping(value="/allCategorie")
    public List<Categorie> allCategorie(){
        return categorieRepository.findAll();
    }

    @PostMapping(value="/addCategorie")
    @PreAuthorize("hasRole('admin') or hasRole('pm')")
    public Categorie addCategorie(@Valid @RequestBody Categorie c){
        return categorieRepository.save(c);
    }

    @GetMapping(value="/categorie/{id}")
    public ResponseEntity<Categorie> getCategorie(@PathVariable Integer id) throws CategorieIntrouvableException{
        Categorie c = categorieRepository.findById(id);
        if(c==null) throw new CategorieIntrouvableException("La catégorie avec l'id " + id + " est INTROUVABLE. ");
        else
        return ResponseEntity.ok().body(c);
    }

    @PutMapping(value="/categorie/{id}")
    @PreAuthorize("hasRole('admin') or hasRole('pm')")
    public ResponseEntity<Categorie> updateCategorie(@PathVariable Integer id,@Valid @RequestBody Categorie catDetails) throws CategorieIntrouvableException{
        Categorie c = categorieRepository.findById(id);
        if(c==null) throw new CategorieIntrouvableException("La catégorie avec l'id " + id + " est INTROUVABLE. ");
        else
        c.setNom(catDetails.getNom());
        Categorie updateCat = categorieRepository.save(c);
        return ResponseEntity.ok(updateCat);
    }

    @DeleteMapping(value="/categorie/{id}")
    @PreAuthorize("hasRole('admin') or hasRole('pm')")
    public Map<String,Boolean> deleteCategorie(@PathVariable Integer id) throws CategorieIntrouvableException{
        Categorie c = categorieRepository.findById(id);
        if(c==null) throw new CategorieIntrouvableException("La catégorie avec l'id " + id + " est INTROUVABLE. ");
        else
        categorieRepository.delete(c);
        Map<String,Boolean> response = new HashMap<>();
        response.put("Catégorie est supprimé!",Boolean.TRUE);
        return response;
    }
}
