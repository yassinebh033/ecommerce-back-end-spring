package com.example.ecomerce.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.ecomerce.dao.ProductRepository;
import com.example.ecomerce.model.Product;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(description = "gestion des produit")
@RestController
@RequestMapping(value = "/rest/api")
@CrossOrigin(origins = "http://localhost:4200")
public class ProduitController {
	@Autowired
	ProductRepository produitRepository;

	@ApiOperation("consulter tout les produits")
	@GetMapping(value = "/allProduit")
	public List<Product> allProduit() {
		return produitRepository.findAll();
	}

	@GetMapping(value = "/produit/{id}")
	public ResponseEntity<Product> produit(@PathVariable Integer id) throws Exception {
		final Product produit = produitRepository.findById(id)
				.orElseThrow(() -> new Exception("Le produit n'existe pas"));
		return ResponseEntity.ok().body(produit);
	}

	@ApiOperation("consulter tout les produits")
	@PostMapping(value = "/addProduit")
	@PreAuthorize("hasRole('admin') or hasRole('pm')")
	public Product addProduit(@Valid @RequestBody Product produit) {
		return produitRepository.save(produit);
	}

	@PutMapping(value = "/produit/{id}")
	@PreAuthorize("hasRole('admin') or hasRole('pm')")
	public ResponseEntity<Product> updateProduit(@PathVariable Integer id, @Valid @RequestBody Product produitDetails)
			throws Exception {
		Product produit = produitRepository.findById(id).orElseThrow(() -> new Exception("Le produit n'existe pas"));
		produit.setNom(produitDetails.getNom());
		produit.setPrixAchat(produitDetails.getPrixAchat());
		produit.setPrix(produitDetails.getPrix());
		produit.setCategorie(produitDetails.getCategorie());
		produitRepository.save(produit);
		return ResponseEntity.ok(produit);
	}

	@DeleteMapping(value = "/produit/{id}")
	@PreAuthorize("hasRole('admin') or hasRole('pm')")
	public Map<String, Boolean> deleteProduit(@PathVariable Integer id) throws Exception {
		Product produit = produitRepository.findById(id).orElseThrow(() -> new Exception("Le produit n'existe pas"));
		produitRepository.delete(produit);
		Map<String, Boolean> response = new HashMap<>();
		response.put("Le produit est supprimé!", Boolean.TRUE);
		return response;
	}

	@PutMapping(value = "/produit/{id}/{prix}")
	@PreAuthorize("hasRole('admin') or hasRole('pm')")
	public void updatePrix(@PathVariable Integer id, @PathVariable Double prix) {
		produitRepository.updatePrix(id, prix);
	}

}
